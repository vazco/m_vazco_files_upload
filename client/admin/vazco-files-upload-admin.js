'use strict';

Template.vazcoAdminNav_resizeImages.helpers({
    resizeImagesCollections: function () {
        return _.map(FS._collections, function (item) {
            return {
                random: Random.hexString(5),
                name: item.name
            };
        });
    },
    isDisabled: function (e, tmpl) {
        return Session.get('adminResizeImagesBtnDisabled');
    }
});

Template.vazcoAdminNav_resizeImages.events({
    'click .resizeImagesBtn': function (e, tmpl) {
        e.preventDefault();
        var collection = tmpl.$('[name=resize-images-radio]:checked').val();
        if (collection) {
            tmpl.$('.resizeImagesBtn .load').removeClass('hide');
            Meteor.call('VazcoFilesUploadResize', collection, function (error) {
                if (error) {
                    console.log(error);
                } else {
                    tmpl.$('.resizeImagesBtn .load').addClass('hide');
                    console.log('Ok! Images resized!');
                };
            });
        }
    },
    'click [name=resize-images-radio]': function (e, tmpl) {
        e.preventDefault();
        if(tmpl.$('[name=resize-images-radio]').is(':checked')) {
            Session.set('adminResizeImagesBtnDisabled', false);
        } else {
            Session.set('adminResizeImagesBtnDisabled', true);
        }
    }
});

Template.vazcoAdminNav_resizeImages.rendered = function () {
    Session.set('adminResizeImagesBtnDisabled', true);
};