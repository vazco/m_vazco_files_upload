## Vazco Files Upload with CollectionFS

You need packages:

- mrt add autoform
- mrt add collectionFS
- mrt add cfs-filesystem
- mrt add ui-dropped-event
- mrt add cfs-graphicsmagick
- mrt add cfs-ui

and **git@bitbucket.org:vazco/m_vazco_files_upload.git** as a git submodule

and also in **.meteor/packages** before collectionFS add **vazco-files-upload**

### Usage

You can configure two parts of upload mechanism. First is store mechanism, which define how files will be stored, the second one is filter, which define extensions, max size and allowed content type of file.

#### Files collection initialization example:

Basic usage:

````
(Meteor.isClient ? window : global).Files = VazcoFilesUpload.init({
    name: 'files',
});
````

Also You need to set your allow map - example:

````
Files.allow({
    insert: function () { // userId, doc
        return true;
    },
    update: function () { // userId, doc, fields, modifier
        return true;
    },
    remove: function () { // userId, doc
        return true;
    },
    download: function () {
        return true;
    }
});
````

### Stores and filters

By default we use two default stores, and filter preset which looks like:

1. Filters
````
{
	maxSize: 50000000,
	allow: {
		contentTypes: ['image/*'],
		extensions: ['jpg', 'jpeg', 'png']
	}
};
````

2. Stores:
````
VazcoFilesUploadStores.add('files');
VazcoFilesUploadStores.add('thumbs', {
	'crop': {
		'width': 140,
		'height': 190,
		'gravity': 'center'
	}
});
````

You may use you own stores and filters. We use stores and filter sets, that means, you can set only once preset for stores or filters, and then use it for any collections:

Example:
````
VazcoFilesUploadFilters.add('my_filters_preset_name', {
	maxSize: 70000000,
	allow: {
		contentTypes: ['image/*'],
		extensions: ['jpg', 'jpeg', 'png', 'gif']
	}
});

VazcoFilesUploadStores.add('files');
VazcoFilesUploadStores.add('thumbs', {
	'crop': {
		'width': 160,
		'height': 160,
		'gravity': 'center'
	}
});
VazcoFilesUploadStores.add('special_thumbs', {
	'crop': {
		'width': 160,
		'height': 160,
		'gravity': 'center'
	}
});
````

and then create new collections which your presets:

````
(Meteor.isClient ? window : global).FilesOne = VazcoFilesUpload.init({
    name: 'FilesOne',
    stores: ['files', 'special_thumbs'], // use my two stores
    filter: 'my_filters_preset_name' // use my custom preset
});

(Meteor.isClient ? window : global).FilesTwo = VazcoFilesUpload.init({
    name: 'FilesOne',
    stores: ['files'], // use only one store
});
````

You can also choose custom storage adapter.
You need to first add it to project, and then during init pass it as third param, e.g:

```
VazcoFilesUploadStores.add('name', {}, 'GridFS');
```

If you want to set storage adapter for default storages you need to pass
it in settings file as `Meteor.settings.public.VFU.storage`

#### Template example:

````

<template name="filesSystemUpload">

  {{> vazcoFilesUploadDropzone files=files classSufix='files'}}

</template>
````

- **files** - files data
- **classSufix** - needed for differentiate many drop areas (vfu-dropzone-{{classSufix}} class and hidden file input class - vfu-hidden-input-{{classSufix}} )

#### or with custom html:

````
<template name="filesSystemUpload">

  {{#vazcoFilesUploadDropzone}}
    <div class="vfu-dropzone-files2 vfu-dropzone">
      <div class="vfu-list test-class">
        {{#each files2}}
          {{#if isImage}}
            <div class="vfu-single-file">
              <a href="{{this.url store='files'}}">
                <img id="{{_id}}" src="{{this.url store='thumbs'}}" alt="">
              </a>
              {{#FS.DeleteButton class="btn btn-danger btn-xs vfu-delete-file"}}
                <span class="glyphicon glyphicon-remove"></span>
              {{/FS.DeleteButton}}
              {{#unless this.isUploaded}}
                <div class="">
                  {{> FS.UploadProgressBar bootstrap=true}}
                </div>
              {{/unless}}
            </div>
          {{else}}
            <div class="vfu-single-file">
              <a href="{{this.url store='files'}}" class="vfu-no-image-file" title="{{this.name}}">
                <span class="glyphicon glyphicon-file"></span>
                <span class="vfu-file-name">{{this.name}}</span>
              </a>
              {{#FS.DeleteButton class="btn btn-danger btn-xs vfu-delete-file"}}
                <span class="glyphicon glyphicon-remove"></span>
              {{/FS.DeleteButton}}
              {{#unless this.isUploaded}}
                <div class="">
                  {{> FS.UploadProgressBar bootstrap=true}}
                </div>
              {{/unless}}
            </div>
          {{/if}}
        {{else}}
          <div class="vfu-label">
              Drop files to upload <br>
              (or click)
          </div>
        {{/each}}
      </div>
    </div>
    <input type="file" multiple="multiple" class="vfu-hidden-input vfu-hidden-input-files2">
  {{/vazcoFilesUploadDropzone}}

</template>
````

#### Events

Insert file function:

**VazcoFilesUpload.insertFile(collection, entity, config);**

- **collection** - files collection instance
- **entity** - custom string id (ex. form name, id etc) can be used for associating many forms with just uploaded files
- **config** - metadata object and after callback function

**Example:**

````
Template.filesSystemUpload.events({
  'dropped .vfu-dropzone-files': function(e) {
    VazcoFilesUpload.insertFile(Files, 'myForm', {
      metadata: {
        owner: Meteor.userId(),
        // filesFor: 'myForm'
        // other fields you need
      },
      after: function(error, fileObj) {
        console.log('Inserted file name:', fileObj.name());
      }
    }, e);
  },
  'change .vfu-hidden-input-files': function (e) {
    VazcoFilesUpload.insertFile(Files, 'myForm', {
      metadata: {
        owner: Meteor.userId()
      },
      after: function(error, fileObj) {
        console.log('Inserted file name:', fileObj.name());
      }
    }, e);
  }
});
````

**After files upload you will have session variable ex.: 'justUploadedFilesFor_myForm' with array of files objects you can use this for files metadata updates after form submit. Session variable is just 'justUploadedFilesFor_' + entity (you pass in insertFile function).**

#### Resize all files in collection

If you want to change your thumbnails sizes you should resize all files in collection by:
````
Meteor.call('VazcoFilesUploadResize', collection, function (error) {
    if (error) {
        console.log(error);
    } else {
        console.log('Ok! Images resized!');
    };
});
````
**collection** - collection name configured in VazcoFilesUpload.init
````
VazcoFilesUpload.init({
  [...]
  name: 'collectionNameToPass'
  [...]
});
````

This is also compatible and attached into VazcoAdmin (by adminPluginsNavs)

#### CSS

You can overwrite some of the main css classes:

- **.vfu-dropzone** - main dropzone container
- **.vfu-dragover** - drag over class
and other, all with 'vfu' prefix
