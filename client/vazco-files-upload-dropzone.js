'use strict';

Template.vazcoFilesUploadDropzone.events({
    'click .vfu-dropzone': function (e, tmpl) {
        e.stopPropagation();
        tmpl.$('.vfu-hidden-input').trigger('click');
    },
    'click .vfu-single-file': function (e) {
        e.stopPropagation();
    },
    'dragenter .vfu-dropzone': function (e, tmpl) {
        $('.vfu-dropzone-overlay').addClass('vfu-hide');
        tmpl.$('.vfu-label').addClass('vfu-hide');
        tmpl.$('.vfu-dropzone-overlay').removeClass('vfu-hide');
        tmpl.$('.vfu-dropzone').addClass('vfu-dragover');
    },
    'dragleave .vfu-dropzone-overlay': function (e, tmpl) {
        tmpl.$('.vfu-dropzone-overlay').addClass('vfu-hide');
        tmpl.$('.vfu-label').removeClass('vfu-hide');
        tmpl.$('.vfu-dropzone').removeClass('vfu-dragover');
    },
    'drop .vfu-dropzone': function (e, tmpl) {
        tmpl.$('.vfu-dropzone-overlay').addClass('vfu-hide');
        tmpl.$('.vfu-dropzone').removeClass('vfu-dragover');
    }
});