'use strict';

VazcoFilesUploadStores = (function () {

    var _stores_storage = {};
    var stores_thumbs_sizes = {};

    /**
     *
     * @param storyName
     * @param story
     * @private
     */
    var _add = function (storyName, story, sizes) {
        _stores_storage[storyName] = story;
        stores_thumbs_sizes[storyName] = sizes;
    };

    /**
     * Get story from base
     *
     * @param storyName
     * @returns {*}
     * @private
     */
    var _get = function (storyName) {
        return _stores_storage[storyName];
    };

    var getThumbsSizes = function (storyName) {
        return stores_thumbs_sizes[storyName];
    };

    return {
        /**
         * Get story thumbs sizes
         *
         * @param storyName
         * @returns {Object}
         */
        sizes: getThumbsSizes,

        /**
         * Check if story exists or not
         *
         * @param storyName
         * @returns {boolean}
         */
        exists: function (storyName) {
            return (storyName in _stores_storage);
        },

        /**
         * Add new story to our story base
         * @param storyName
         * @param storyOptions
         * @param {String} [storageAdapter='FileSystem']
         */
        add: function (storyName, storyOptions, storageAdapter) {
            var StorageAdapterClass;
            if(!storageAdapter){
                StorageAdapterClass = FS.Store.FileSystem;
            } else {
                StorageAdapterClass = FS.Store[storageAdapter];
            }

            if(!StorageAdapterClass){
                throw new Meteor.Error('500', 'Invalid Storage Adapter');
            }

            if (!VazcoFilesUploadStores.exists(storyName)) {
                var story, thumbSizes = thumbSizes || {};
                if (!storyOptions) {
                    story = new StorageAdapterClass(storyName);
                } else {

                    // Create new story
                    story = new StorageAdapterClass(storyName, {
                        transformWrite: function (fileObj, readStream, writeStream) {

                            // Manipulate on image file only!
                            if (fileObj.isImage()) {

                                // Get file from stream
                                var g = gm(readStream);

                                // Update only main story
                                var size = Meteor.wrapAsync(g.size.bind(g));
                                size(function (err, result) {
                                    var fileObjResolution = fileObj.original.resolution;
                                    if (!_.isObject(fileObjResolution)) {
                                        fileObj.update({$set: {'original.resolution': result}});
                                    }
                                });

                                // Crop image
                                if (_.isObject(storyOptions) && 'crop' in storyOptions) {
                                    var width = storyOptions.crop.width;
                                    var height = storyOptions.crop.height;
                                    var gravity = storyOptions.crop.gravity || 'center';

                                    if (_.isNumber(width) && _.isNumber(height) && width > 0 && height > 0) {
                                        g.thumbnail(width + '^', height + '^').gravity(gravity).crop(width, height);
                                    } else {
                                        console.log('Can not crop to dimensions: ', width, ' x ', height);
                                    }
                                }

                                // Save file
                                g.stream().pipe(writeStream);

                            }

                        }
                    });
                }
                // Add new story and story sizes to our storage
                if (_.isObject(storyOptions) && 'crop' in storyOptions) {
                    thumbSizes.width = storyOptions.crop.width;
                    thumbSizes.height = storyOptions.crop.height;
                }
                _add(storyName, story, thumbSizes);
            }
        },

        /**
         * Get stores to use with FS Collection
         *
         * @param storesNamesArray
         * @returns {Array}
         */
        use: function (storesNamesArray) {

            var storesToReturn = [];
            storesNamesArray.forEach(function (storyName) {

                if (VazcoFilesUploadStores.exists(storyName)) {
                    storesToReturn.push(_get(storyName));
                }
            });

            return storesToReturn;
        },

        /**
         * Backward capability - use default stores
         *
         * @returns {string[]}
         */
        useDefault: function () {

            var storageAdapter = 'FileSystem';
            if(Meteor.settings.public && Meteor.settings.public.VFU && Meteor.settings.public.VFU.storage){
                storageAdapter = Meteor.settings.public.VFU.storage;
            }

            VazcoFilesUploadStores.add('files', null, storageAdapter);
            VazcoFilesUploadStores.add('thumbs', {
                'crop': {
                    'width': 160,
                    'height': 160,
                    'gravity': 'center'
                }
            }, storageAdapter);

            // Return this stores
            return VazcoFilesUploadStores.use(['files', 'thumbs']);

        }
    };
})();
