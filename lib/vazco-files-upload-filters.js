'use strict';

VazcoFilesUploadFilters = (function () {

    var _filters_storage = {};

    /**
     *
     * @param filterName
     * @param filter
     * @private
     */
    var _add = function (filterName, filter) {
        _filters_storage[filterName] = filter;
    };

    /**
     * Get filter from base
     *
     * @param filterName
     * @returns {*}
     * @private
     */
    var _get = function (filterName) {
        return _filters_storage[filterName];
    };

    return {

        /**
         * Check if filter exists or not
         *
         * @param filterName
         * @returns {boolean}
         */
        exists: function (filterName) {
            return (filterName in _filters_storage);
        },

        /**
         * Add new filter to our filter base
         * @param filterName
         * @param filterOptions
         */
        add: function (filterName, filterOptions) {

            if (!VazcoFilesUploadFilters.exists(filterName) && _.isObject(filterOptions) && !_.isEmpty(filterOptions)) {

                // prepare filter array


                var filter_options = {
                    maxSize: filterOptions.maxFileSize
                };

                if(filterOptions.contentTypes || filterOptions.extensions){
                    filter_options.allow = {
                        contentTypes: filterOptions.contentTypes,
                        extensions: filterOptions.extensions
                    };
                }

                // add filter array to our base
                _add(filterName, filter_options);
            }
        },

        /**
         * Get filter preset to use
         *
         * @param filterPresetName
         * @returns {Array}
         */
        use: function (filterPresetName) {
            return _get(filterPresetName);
        },


        /**
         * Backward capability - use default filters
         *
         * @returns {string[]}
         */
        useDefault: function () {

            // Return default filters
            return {
                maxSize: 50000000,
                allow: {
                    contentTypes: ['image/*'],
                    extensions: ['jpg', 'jpeg', 'png']
                }
            };

        }
    }
})();