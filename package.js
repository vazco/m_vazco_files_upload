'use strict';

Package.describe({
  name:     'vazco:files-upload',
  version:  '0.1.2',
  summary: 'Files uploads with collectionFS'
});

Package.on_use(function (api) {

  if (api.versionsFrom) {
    api.versionsFrom('METEOR@0.9.0');
      api.use([
        'aldeed:autoform',
        'cfs:standard-packages',
        'cfs:filesystem',
        'cfs:dropped-event',
        'cfs:graphicsmagick',
        'cfs:ui'
      ]);

      api.use('cfs:gridfs', ['client', 'server'], {weak: 'true'});

      api.imply([
        'aldeed:autoform',
        'cfs:standard-packages',
        'cfs:filesystem',
        'cfs:dropped-event',
        'cfs:graphicsmagick',
        'cfs:ui'
      ]);
    } else {
      api.imply([
        'autoform',
        'collectionFS',
        'cfs-filesystem',
        'ui-dropped-event',
        'cfs-graphicsmagick',
        'cfs-ui'
      ]);
  }

  api.use(['underscore'], ['client', 'server']);
  api.use(['templating', 'ui'], 'client');

  api.add_files([
    'lib/vazco-files-upload.js',
    'lib/vazco-files-upload-stores.js',
    'lib/vazco-files-upload-filters.js'
  ], ['client', 'server']);

  api.add_files([
    'client/vazco-files-upload-dropzone.css',
    'client/vazco-files-upload-dropzone.html',
    'client/vazco-files-upload-dropzone.js',
    'client/admin/vazco-files-upload-admin.html',
    'client/admin/vazco-files-upload-admin.js'
  ], 'client');

  api.add_files([
    'server/vazco-files-upload-resize.js'
  ], ['server']);

  api.export([
    'VazcoFilesUpload',
    'VazcoFilesUploadStores',
    'VazcoFilesUploadFilters'
  ], ['client', 'server']);

});
