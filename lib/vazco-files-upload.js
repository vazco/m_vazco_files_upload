'use strict';
/* jshint -W020 */

VazcoFilesUpload = (function () {
    /**
     * ========================
     * PUBLIC API
     * ========================
     */
    return {
        /**
         * Upload new file
         *
         * @param collection
         * @param entity
         * @param config
         * @param e {mouse event}
         * @returns {undefined}
         */
        insertFile: function (collection, entity, config, e) {
            config = config || {};

            var metadata = config.metadata;
            var callback = config.after;
            var justUploadedFiles = Session.get('justUploadedFilesFor_' + entity) || [];
            var insertedFile;
            return FS.Utility.eachFile(e, function (file) {
                var newFile = new FS.File(file);
                newFile.metadata = metadata;
                if (callback) {
                    insertedFile = collection.insert(newFile, callback);
                    justUploadedFiles.push(insertedFile);
                } else {
                    insertedFile = collection.insert(newFile);
                    justUploadedFiles.push(insertedFile);
                }
                justUploadedFiles = _.filter(justUploadedFiles, function (item) {
                    if(item){
                        return collection.findOne(item._id);
                    }
                });
                Session.set('justUploadedFilesFor_' + entity, justUploadedFiles);
            });
        },

        /**
         * Init new FS Collection
         *
         * @param config
         * @returns {FS.Collection}
         */
        init: function (config) {

            var _stores, _filter;

            // Get stores to use
            if ('stores' in config && _.isArray(config.stores)) {
                _stores = VazcoFilesUploadStores.use(config.stores);
            } else {
                _stores = VazcoFilesUploadStores.useDefault();
            }

            // Get filter to use
            if (!config.noFilters) {
                if ('filter' in config && _.isString(config.filter)) {
                    _filter = VazcoFilesUploadFilters.use(config.filter);
                    if (!_.isObject(_filter)) {
                        _filter = VazcoFilesUploadFilters.useDefault();
                    }
                } else {
                    _filter = VazcoFilesUploadFilters.useDefault();
                }
            }

            var params = {};

            if (_stores) {
                params.stores = _stores;
            }
            if (_filter) {
                params.filter = _filter;
            }

            return new FS.Collection(config.name, params);
        }
    };
})();