'use strict';

Meteor.methods({
    VazcoFilesUploadResize: function (collection) {
        if (Meteor.user() && Meteor.user().is_admin) {
            var collection = FS._collections[collection],
                primaryStore = collection.primaryStore.name,
                stores = _.pluck(collection.options.stores, 'name');
                stores = _.filter(stores, function (store) {
                    return store !== primaryStore;
                });
            _.each(stores, function (store) {
                collection.find().forEach(function (fileObj) {
                    if (fileObj.isImage()) {
                        var readStream = fileObj.createReadStream(primaryStore),
                            writeStream = fileObj.createWriteStream(store),
                            thumbWidth = VazcoFilesUploadStores.sizes(store).width,
                            thumbHeight = VazcoFilesUploadStores.sizes(store).height,
                            g = gm(readStream);
                        g.thumbnail(thumbWidth + '^', thumbHeight + '^').gravity('Center').crop(thumbWidth, thumbHeight);
                        g.stream().pipe(writeStream);
                    }
                });
            });
        } else {
            throw new Meteor.Error(403, 'Access forbidden');
        }
    }
});
